import itertools
import os
import winreg

import psutil

import win32security as ws


def kill_steam():
    PROCNAME = "steam.exe"
    for proc in psutil.process_iter():
        if proc.name() == PROCNAME:
            proc.kill()


def get_sid(name=os.getlogin()):
    sid = ws.LookupAccountName(None, name)[0]
    sidstr = ws.ConvertSidToStringSid(sid)
    return sidstr


KEY_READ_64 = winreg.KEY_READ | winreg.KEY_WOW64_64KEY
ERROR_NO_MORE_ITEMS = 259


def iterkeys(key):
    for i in itertools.count():
        try:
            yield winreg.EnumKey(key, i)
        except OSError as e:
            if e.winerror == ERROR_NO_MORE_ITEMS:
                break
            raise


def itervalues(key):
    for i in itertools.count():
        try:
            yield winreg.EnumValue(key, i)
        except OSError as e:
            if e.winerror == ERROR_NO_MORE_ITEMS:
                break
            raise


STEAN = r"SOFTWARE\Valve\Steam"


def printNets(keystr=STEAN):
    # CURRENT_USER = (winreg.HKEY_USERS + get_sid())
    key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, keystr, 0, KEY_READ_64)
    print(f"key {key}")
    for guid in iterkeys(key):
        netKey = winreg.OpenKey(key, guid)
        print(netKey)
        for name, data, rtype in itervalues(netKey):
            if type(data) is str:
                print(f"String {name}: {data}")
                continue
            try:
                iterator = iter(data)
            except TypeError:
                print(f"Nieiter {name}: {data}")
                continue
            # for id in iterkeys(data):
            #     insidekey = OpenKey(data, id)
            #     for name, data, rtype in insidekey:
            #         print(name)
            #         print("****************")


printNets()
